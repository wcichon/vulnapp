import os, os.path

import cherrypy
import csv
import sqlite3
from sqlite3 import Error
import time
import Jwt


def readFile(path):
    f = open(path, "r")
    contents = f.read()
    return contents


def dbHelper(query, rowmapper):
    try:
        conn = sqlite3.connect("database.db")
        cur = conn.cursor()

        print(query)
        cur.execute(query)
        rows = cur.fetchall()
        result = []
        for row in rows:
            result.append(rowmapper(row))
        return result
    except Error as e:
        return str(e)
    finally:
        conn.close()


def keyProvider(kid):
    # TODO read keys from secure storage
    keys = {
        1: b'secret'
    }
    return keys.get(kid, None)


class MyService:
    jwt = Jwt.JWT()

    @cherrypy.expose
    def index(self):
        if 'token' in cherrypy.session.keys():
            cookie = cherrypy.request.cookie
            jws = cookie['token'].value
            kid = self.jwt.getHeader(jws, 'kid')
            result = self.jwt.decode(jws, keyProvider(kid))
            if result[0] == False:
                self.logout()
            token = result[1]
            if (token['role'] == 'admin'):
                uid = '0'
            else:
                uid = token['uid']
            return readFile("static/page.html").replace('USER_ID', uid)
        return readFile("static/index.html")

    @cherrypy.expose
    def login(self, user, password):
        query = "SELECT * FROM tblusers where fname = '" + user + "' and password='" + password + "' LIMIT 1;"
        users = dbHelper(query, lambda row: {'id': str(row[0]), 'fname': row[1]})
        if len(users) == 1:
            user = users[0]
            exp = int(time.time()) + 3600;
            payload = {'uid': user['id'], 'uname': user['fname'], 'exp': exp, 'role': 'user'};
            header = {'kid': 1}
            token = self.jwt.encode(payload, keyProvider(1), header=header);
            cookie = cherrypy.response.cookie
            cookie['token'] = token
            cookie['token']['path'] = '/'
            cookie['token']['max-age'] = 3600
            cookie['token']['version'] = 1
            cherrypy.session['token'] = token
        # return "logged"
        raise cherrypy.HTTPRedirect('/')

    @cherrypy.expose
    def logout(self):
        del cherrypy.session['token']
        raise cherrypy.HTTPRedirect('/')

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def data(self, userid, search=""):
        conn = sqlite3.connect("database.db")
        cur = conn.cursor()
        search = search.upper()
        query = "SELECT * FROM tbltx tx where (tx.desc like '%" + search + "%')"
        if (userid != '0'):
            query += " and (tx.uid = " + userid + ") "

        return dbHelper(query, lambda row: {'id': row[0], 'uid': row[1], 'desc': row[2], 'amount': row[3]})


def _cp_dispatch(self, vpath):
    if vpath[0] == 'data':
        cherrypy.request.params['userid'] = vpath[1]
    return vpath


if __name__ == '__main__':
    conf = {

        '/res':
            {'tools.staticdir.on': True,
             'tools.staticdir.dir': "./static/res"
             },
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd()),
            # 'tools.auth_basic.checkpassword': validate_password,

        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './static'
        }
    }
    cherrypy.config.update({'server.socket_host': '0.0.0.0'})
    cherrypy.quickstart(MyService(), '/', conf)
