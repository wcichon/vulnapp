import base64
import hashlib
import hmac
import json


class Algorithm(object):
    def verify(self, token, key):
        raise NotImplementedError

    def sign(self, payload, key):
        raise NotImplementedError


class NoAlgo(Algorithm):
    def verify(self, token, key):
        return True

    def sign(self, payload, key):
        return "";


class Hmac256(Algorithm):
    def verify(self, token, key):
        parts = token.split(".")
        sig_bytes =parts[2];
        sig_ver = self.sign(parts[0] +'.'+parts[1], key)
        ver_bytes = sig_ver;
        return sig_bytes == ver_bytes

    def sign(self, payload, key):
        dig = hmac.new(key, msg=payload.encode("ascii"), digestmod=hashlib.sha256).digest()
        return base64.b64encode(dig).decode();

class JWT:
    algorithms = {
        "none": NoAlgo(),
        "hs256": Hmac256()
    }



    def encode(self,body,secret,alg="hs256",header = {}):
        header['alg'] = alg
        h=base64.urlsafe_b64encode( json.dumps(header).encode("ascii")).decode("ascii")
        jsonStr = json.dumps(body);
        b=base64.urlsafe_b64encode( jsonStr.encode("ascii")).decode("ascii")
        payload = h+"."+b
        algorithm = self.algorithms.get(alg.lower())
        return payload+"."+algorithm.sign(payload,secret)

    def decode(self,token,secret):
        parts = token.split(".")
        if len(parts)!=3:
            return (False,None)
        tokenb64 = base64.b64decode(parts[0])
        header = json.loads(tokenb64)
        alg = header['alg']
        algorithm = self.algorithms.get(alg.lower())

        if (algorithm.verify(token,secret)):
            return (True,json.loads(base64.b64decode(parts[1])))
        return (False, None)

    def getHeader(self, token,key):
        parts = token.split(".")
        tokenb64 = base64.b64decode(parts[0])
        header = json.loads(tokenb64)
        print(header)
        return header[key]